BASE_DIR=$$(readlink -e "$$(pwd)")
BIN_DIR=$(BASE_DIR)/../bin/
LOG_DIR=$(BASE_DIR)/../log/
SRC_DIR=$(BASE_DIR)/../src/
TEX_DIR=$(BASE_DIR)/../tex/
TMP_DIR=$(BASE_DIR)/../tmp/
TMP_DIR2=$(BASE_DIR)/tmp/

TEX_FILENAME=book*

TEX=perltex -latex=lualatex
TEXDEBUGFLAGS=
TEXFLAGS=--interaction=batchmode $(TEXDEBUGFLAGS)

ARGS=($(MAKECMDGOALS))

.ONESHELL:
latex-debug:
	@echo "Compiling report in debug mode started"
	@ls $(TMP_DIR) || mkdir $(TMP_DIR)
	@cd $(TMP_DIR2)
	@for TEX_FILE in "$(TEX_DIR)"$(TEX_FILENAME).tex; do \
	    $(TEX) $(TEXDEBUGFLAGS) "$$TEX_FILE"; \
	    $(TEX) $(TEXDEBUGFLAGS) "$$TEX_FILE"; \
	done
	@for IDX_FILE in "$(TMP_DIR)"$(TEX_FILENAME).idx; do \
	    echo "$$IDX_FILE"; \
	    makeindex "$$(realpath --relative-to "$$(pwd)" "$$IDX_FILE")"; \
	done
	@for TEX_FILE in "$(TEX_DIR)"$(TEX_FILENAME).tex; do \
	    $(TEX) $(TEXDEBUGFLAGS) "$$TEX_FILE"; \
	done
	@mv "$(TMP_DIR)"$(TEX_FILENAME).pdf "$(BIN_DIR)"
	@echo "Compiling report in debug mode ended"

latex-clean:
	@echo "Removing compiled report started"
	@rm -f "$(TMP_DIR)"$(TEX_FILENAME)
	@rm -f "$(BIN_DIR)"$(TEX_FILENAME).pdf
	@echo "Removing compiled report ended"

all: latex-debug

clean: latex-clean
